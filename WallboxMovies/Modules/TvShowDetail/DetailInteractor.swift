//
//  DetailInteractor.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import Foundation


class DetailInteractor: DetailInteractorProtocol {
    var presenter: DetailPresenterProtocol?
    var tvShow: Show?
}
