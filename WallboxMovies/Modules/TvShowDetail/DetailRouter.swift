//
//  DetailRouter.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit

class DetailRouter: DetailRouterProtocol {
    
    static func createDetailViewController(with tvShow: Show) -> UIViewController {
        
        let detailViewController = DetailViewController(nibName: "DetailViewController" , bundle: nil)
        
        let presenter: DetailPresenterProtocol  = DetailPresenter()
        presenter.tvShow = tvShow
        detailViewController.presenter = presenter
        presenter.view = detailViewController
        let interactor: DetailInteractorProtocol  = DetailInteractor()
        interactor.presenter = presenter
        presenter.interactor = interactor
        let router: DetailRouterProtocol  = DetailRouter()
        presenter.router = router
        
        
        return detailViewController
    }
    
}
