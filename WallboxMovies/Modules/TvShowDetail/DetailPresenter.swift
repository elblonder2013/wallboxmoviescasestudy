//
//  DetailPresenter.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import Foundation

class DetailPresenter: DetailPresenterProtocol {
    
    weak var view: DetailViewProtocol?
    var interactor: DetailInteractorProtocol?
    var router: DetailRouterProtocol?
    var tvShow: Show?
    
    func viewDidLoad() {
        view?.configView()
    }
}
