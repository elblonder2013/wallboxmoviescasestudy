//
//  DetailViewController.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit
import SDWebImage
class DetailViewController: UIViewController {
    
    @IBOutlet weak var thumbImageView:UIImageView!
    @IBOutlet weak var backgroundImageView:UIImageView!
    @IBOutlet weak var genreLabel:UILabel!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var rateLabel:UILabel!
    @IBOutlet weak var sumaryLabel:UILabel!
    @IBOutlet  var startsView:UIView!
    @IBOutlet  var rateImageViews:[UIImageView]!
    
    var presenter: DetailPresenterProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension DetailViewController: DetailViewProtocol {

    func configView(){
        guard let tvShow = presenter?.tvShow  else {return}
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationItem.largeTitleDisplayMode = .never
        
        //SETUP THE MOVIE INFO
        navigationItem.title = tvShow.name
        nameLabel.text = tvShow.name
        genreLabel.text = tvShow.genresFormatted
        sumaryLabel.text  = tvShow.summary?.htmlToString()
        
        //SETUP THE BACKGROUND IMAGEVIEW AND COVER
        if let mediumImage = tvShow.mediumImage{
            thumbImageView.sd_setImage(with: URL(string: mediumImage), completed: nil)
            backgroundImageView.sd_setImage(with: URL(string: mediumImage), completed: nil)
        }
        else if  let originalImage = tvShow.originalImage{
            thumbImageView.sd_setImage(with: URL(string: originalImage), completed: nil)
            backgroundImageView.sd_setImage(with: URL(string: originalImage), completed: nil)
        }
        
        //SETUP THE AVERAGE
        if let average = tvShow.rating.average {
            startsView.alpha = 1
            rateLabel.text = "\(average)"
            let ave = Int(average * 5 / 10)
            for i in 0..<rateImageViews.count{
                rateImageViews[i].image = (i + 1) <= ave ? #imageLiteral(resourceName: "start_fill"):#imageLiteral(resourceName: "start_empty")
            }
        } else {
            startsView.alpha = 0.3
            rateLabel.text = "-"
            for i in 0..<rateImageViews.count{
                rateImageViews[i].image = #imageLiteral(resourceName: "start_empty")
            }
        }
    }
}
