//
//  DetailProtocols.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit

protocol DetailViewProtocol: class {
    
    var presenter: DetailPresenterProtocol? {get set}
    func configView()
}

protocol DetailPresenterProtocol: class {
    
    var view: DetailViewProtocol? { get set }
    var interactor: DetailInteractorProtocol? { get set }
    var router: DetailRouterProtocol? { get set }
    var tvShow: Show? {get set}
    func viewDidLoad()

}

protocol DetailInteractorProtocol: class {
    var presenter: DetailPresenterProtocol? { get set }
}

protocol DetailRouterProtocol: class {
    static func createDetailViewController(with tvShow: Show) -> UIViewController
}
