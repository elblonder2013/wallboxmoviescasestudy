//
//  HomeViewProtocol.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit

protocol HomeViewProtocol: class {
    var presenter: HomePresenterProtocol? {get set}
    func reloadTvShowList()
    func hideLoading()
    func showErrorAlert(error:String)
}
 
protocol HomePresenterProtocol: TvShowListInteractorOutputProtocol {
    
    var view: HomeViewProtocol? { get set }
    var interactor: HomeInteractorProtocol? { get set }
    var router: HomeRouterProtocol? { get set }
    
    var tvShowList:[Show] { get }
    var tvShowCount: Int { get }
    var curretPage:Int { get }
    func tvShowForIndex(index:Int)->Show
    
    //PRESENTER -> INTERACTOR
    func fetchTvShows()
    func fetchMoreTvShows()
    
    //PRESENTER -> VIEW
    func viewDidLoad()
    
    //PRESENTER -> ROUTER
    func goToDetail(with film: Show)

}


protocol HomeInteractorProtocol: class {
    var presenter: TvShowListInteractorOutputProtocol? { get set }
    func fetchTvShows(at page:Int)
  
}

protocol HomeRouterProtocol: class {
    
    static func createMainViewController() -> UIViewController
    func goToDetail(from view: HomeViewProtocol, with film: Show)
    
}


protocol TvShowListInteractorOutputProtocol: class {
    func onTvShowsFetchSuccess(response: [Show])
    func onTvShowsFetchError(error: String)
}
