//
//  HomeInteractor.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import Foundation

class HomeInteractor: HomeInteractorProtocol {
    
    
    
    weak var presenter: TvShowListInteractorOutputProtocol?
    var gettingMoreShows = false
    
    func fetchTvShows(at page: Int) {
        guard !gettingMoreShows else { return }
        gettingMoreShows = true
        
        let urlString = AppConstans.apiKey + AppEndpoints.shows + "?page=\(page)"
        
        guard let url = URL(string: urlString) else { return print("Error fetching API, no given URL within the function")}
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                self.gettingMoreShows = false
                DispatchQueue.main.async {
                    self.presenter?.onTvShowsFetchError(error: error.localizedDescription)
                }
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                DispatchQueue.main.async {
                    self.presenter?.onTvShowsFetchError(error: "Error with the response, unexpected result. \n response: \(String(describing: response))\n error : \(String(describing: error))")
                    self.gettingMoreShows = false
                }
                return
            }
            
            if let data = data {
                do {
                    let results = try JSONDecoder().decode([Show].self, from: data)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        var d = [Show]()
                        for i in 0..<8{
                            d.append(results[i])
                        }
                        self.presenter?.onTvShowsFetchSuccess(response: d)
                        self.gettingMoreShows = false
                    }
                    /*DispatchQueue.main.async {
                        
                        self.presenter?.onTvShowsFetchSuccess(response: results)
                        self.gettingMoreShows = false
                    }*/
                } catch {
                    DispatchQueue.main.async {
                        self.presenter?.onTvShowsFetchError(error: error.localizedDescription)
                        self.gettingMoreShows = false
                    }
                }
            }
        })
        task.resume()
    }
    
    
    
}
