//
//  HomePresenter.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit

class HomePresenter: HomePresenterProtocol {
    
    weak var view:  HomeViewProtocol?
    var interactor:  HomeInteractorProtocol?
    var router:  HomeRouterProtocol?
    
    var tvShowList: [Show] = []
    var curretPage: Int = 1
    var tvShowCount: Int { tvShowList.count }
    func tvShowForIndex(index:Int)->Show{
        return tvShowList[index]
    }
    
    //PRESENTER -> VIEW
    func viewDidLoad() {
        fetchTvShows()
    }
    
    //PRESENTER -> INTERACTOR
    func fetchTvShows() {
        interactor?.fetchTvShows(at: self.curretPage)
    }
    
    func fetchMoreTvShows(){
        interactor?.fetchTvShows(at: self.curretPage)
    }
    
    //PRESENTER ->ROUTER
    func goToDetail(with tvShow: Show) {
        guard let view = view else {return}
        router?.goToDetail(from: view, with: tvShow)
    }

}
extension HomePresenter: TvShowListInteractorOutputProtocol {
    func onTvShowsFetchSuccess(response: [Show]) {
        view?.hideLoading()
        self.tvShowList.append(contentsOf: response)
        self.curretPage += 1
        view?.reloadTvShowList()
    }
    
    func onTvShowsFetchError(error: String) {
        view?.hideLoading()
        view?.showErrorAlert(error: error)
    }
    
    
}
