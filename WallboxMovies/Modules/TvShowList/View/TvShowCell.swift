//
//  TvShowCell.swift
//  WallboxMovies
//
//  Created by Developer on 28/1/21.
//

import UIKit
import SDWebImage

class TvShowCell: UITableViewCell {
    
    @IBOutlet weak var thumbImageView:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var rateLabel:UILabel!
    @IBOutlet weak var genreLabel:UILabel!
    @IBOutlet  var startsView:UIView!
    @IBOutlet  var rateImageViews:[UIImageView]!
    
    var tvShow: Show? {
        didSet{
            guard tvShow != nil else {return}
            setupCell()
        }
    }
    private func setupCell() {
        guard let tvShow = self.tvShow else { return }
        
        nameLabel.text = tvShow.name
        genreLabel.text = tvShow.genresFormatted
        if let mediumImage = tvShow.mediumImage{
            thumbImageView.sd_setImage(with: URL(string: mediumImage), completed: nil)
        }
        else if  let originalImage = tvShow.originalImage{
            thumbImageView.sd_setImage(with: URL(string: originalImage), completed: nil)
        } else{
            thumbImageView.image = nil
        }
        
        if let average = tvShow.rating.average {
            startsView.alpha = 1
            rateLabel.text = "\(average)"
            let ave = Int(average * 5 / 10)
            for i in 0..<rateImageViews.count{
                rateImageViews[i].image = (i + 1) <= ave ? #imageLiteral(resourceName: "start_fill"):#imageLiteral(resourceName: "start_empty")
            }
        } else{
            startsView.alpha = 0.3
            rateLabel.text = "-"
            for i in 0..<rateImageViews.count{
                rateImageViews[i].image = #imageLiteral(resourceName: "start_empty")
            }
        }
    }
}
