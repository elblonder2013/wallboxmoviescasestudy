//
//  HomeController.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit


class HomeViewController: UIViewController {
    
    var presenter: HomePresenterProtocol?
    var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorStyle = .none
        tv.register(UINib(nibName: "TvShowCell", bundle: nil), forCellReuseIdentifier: "TvShowCell")
        return tv
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = .white
        navigationItem.title = "TV SHOWS"
        setUpView()
        presenter?.viewDidLoad()
        LoadingView.show()
    }
}

extension HomeViewController: HomeViewProtocol {
    
    func reloadTvShowList() {
        tableView.reloadData()
    }
    
    func setUpView() {
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    func hideLoading() {
        LoadingView.hide()
        tableView.tableFooterView = UIView()
    }
    
    func showErrorAlert(error:String) {
        let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Retry", style: .default, handler: {_ in
            if self.presenter?.curretPage == 1{
                LoadingView.show()
            }
            self.presenter?.fetchTvShows()
        }))
        present(alertController, animated: true, completion: nil)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 163
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.tvShowCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tvShow = presenter?.tvShowForIndex(index: indexPath.row)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TvShowCell", for: indexPath) as? TvShowCell
        else {
            return UITableViewCell()
        }
        cell.tvShow = tvShow
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tvShow = presenter?.tvShowForIndex(index: indexPath.row) else {return}
        presenter?.goToDetail(with: tvShow)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}

extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ( (scrollView.contentSize.height - scrollView.contentOffset.y) < scrollView.frame.size.height){
            let footer = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
            let activityIndicator = UIActivityIndicatorView()
            footer.addSubview(activityIndicator)
            activityIndicator.center = CGPoint(x: footer.frame.size.width / 2, y: footer.frame.size.height / 2)
            activityIndicator.startAnimating()
            self.tableView.tableFooterView = footer
            presenter?.fetchMoreTvShows()
            
        }
    }
    
}
