//
//  HomeRouter.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

import UIKit

class HomeRouter:HomeRouterProtocol {
    static func createMainViewController() -> UIViewController {
        let homeViewController = HomeViewController()
        let presenter: HomePresenterProtocol  = HomePresenter()
        homeViewController.presenter = presenter
        presenter.view = homeViewController
        let interactor: HomeInteractorProtocol  = HomeInteractor()
        interactor.presenter = presenter
        presenter.interactor = interactor
        let router: HomeRouterProtocol  = HomeRouter()
        presenter.router = router
    
        return homeViewController
    }
    
    func goToDetail(from view: HomeViewProtocol, with film: Show) {
        let detailViewController = DetailRouter.createDetailViewController(with: film)
        
        guard let mainViewController = view as? HomeViewController else {
            fatalError("Invalid View Protocol type")
        }
        mainViewController.navigationController?.pushViewController(detailViewController, animated: true)
    }
    

}
