//
//  ShowImage.swift
//  WallboxMovies
//
//  Created by alexei on 29/1/21.
//

struct ShowImage:Codable, Equatable {
    var medium:String?
    var original:String?
}
