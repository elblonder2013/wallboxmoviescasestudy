//
//  Show.swift
//  WallboxMovies
//
//  Created by Developer on 27/1/21.
//

struct Show:Codable {
    
    var name:String?
    var image:ShowImage?
    var rating:Rating
    var genres:[String]?
    var summary:String?
    
    //OUPUT, I can make antoher file called ShowsOutput and setup all output values but in this case I setup the 
    var mediumImage:String? { image?.medium }
    var originalImage:String? { image?.original }
    var genresFormatted:String {
        if let genres = genres, genres.count > 0 {
            return genres.joined(separator: ", ")
        } else{
            return ""
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
        case rating = "rating"
        case summary = "summary"
        case genres = "genres"
    }
}
