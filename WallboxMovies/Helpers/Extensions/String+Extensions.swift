//
//  String+Extensions.swift
//  WallboxMovies
//
//  Created by Developer on 29/1/21.
//

import Foundation
extension String {
    func htmlToString()->String {
            var htmlStr = self;
            let scanner:Scanner = Scanner(string: htmlStr);
            var text:NSString? = nil;
            while scanner.isAtEnd == false {
                scanner.scanUpTo("<", into: nil);
                scanner.scanUpTo(">", into: &text);
                htmlStr = htmlStr.replacingOccurrences(of: "\(text ?? "")>", with: "");
            }
            htmlStr = htmlStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            return htmlStr;
    }
}
