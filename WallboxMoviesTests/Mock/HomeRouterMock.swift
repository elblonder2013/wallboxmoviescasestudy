//
//  HomeRouterMock.swift
//  WallboxMoviesTests
//
//  Created by alexei on 2/2/21.
//

import XCTest
@testable import WallboxMovies
class HomeRouterMock: HomeRouterProtocol {
    static func createMainViewController() -> UIViewController {
        return UIViewController()
    }
    var tvShowDetailSuccess: Bool = false
    
    func goToDetail(from view: HomeViewProtocol, with film: Show) {
        tvShowDetailSuccess = true
    }
    
    
    
    
}
