//
//  HomePresenterMock.swift
//  WallboxMoviesTests
//
//  Created by alexei on 2/2/21.
//

import XCTest
@testable import WallboxMovies
class HomePresenterMock: HomePresenterProtocol {
    var view: HomeViewProtocol?
    
    var interactor: HomeInteractorProtocol?
    
    var router: HomeRouterProtocol?
    
    var tvShowList: [Show] = []
    
    var tvShowFetchSuccess: Bool = false
    var tVShowSelected: Bool = false
    var willDisplayCalled: Bool = false
    
    var tvShowCount: Int = 0
    
    var curretPage: Int = 1
    
    func tvShowForIndex(index: Int) -> Show {
        return tvShowList[index]
    }
    
    func fetchTvShows() {
        interactor?.fetchTvShows(at: curretPage)
    }
    
    func fetchMoreTvShows() {
        
    }
    
    func viewDidLoad() {
        
    }
    
    func goToDetail(with film: Show) {
        tVShowSelected = true
        router?.goToDetail(from: view!, with: film)
    }
    
    func onTvShowsFetchSuccess(response: [Show]) {
        tvShowFetchSuccess = true
        self.tvShowList = response
        XCTAssertFalse(response.isEmpty)
        view?.reloadTvShowList()
    }
    
    func onTvShowsFetchError(error: String) {
        tvShowFetchSuccess = false
    }
    
   
}
