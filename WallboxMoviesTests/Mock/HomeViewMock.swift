//
//  HomeViewMock.swift
//  WallboxMoviesTests
//
//  Created by alexei on 29/1/21.
//

import XCTest
@testable import WallboxMovies
class HomeViewMock: HomeViewProtocol {
    var presenter: HomePresenterProtocol?
    var tvShowsLoaded: Bool = false
    var isLoading: Bool = false
    var isError: Bool = false
    
    init(presenter: HomePresenterProtocol) {
        self.presenter = presenter
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadTvShowList() {
        tvShowsLoaded = true
    }
    
    func hideLoading() {
        isLoading = false
    }
    
    func showErrorAlert(error: String) {
        isError = true
    }
    

    

}
