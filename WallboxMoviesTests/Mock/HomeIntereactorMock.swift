//
//  HomeIntereactorMock.swift
//  WallboxMoviesTests
//
//  Created by alexei on 29/1/21.
//


class HomeIntereactorMock: HomeInteractorProtocol {
    var presenter: TvShowListInteractorOutputProtocol?
    var requestCalled: Bool = false
    func fetchTvShows(at page: Int) {
        requestCalled = true
        let tvShow = Show(name: "Test Show", image: ShowImage(medium: "", original: ""), rating: Rating(average: 5.0), genres: ["Comdy"], summary: "summary test")
        self.presenter?.onTvShowsFetchSuccess(response: [tvShow])
    }
}
