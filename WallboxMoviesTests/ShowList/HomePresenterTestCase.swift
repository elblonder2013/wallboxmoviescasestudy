//
//  HomePresenterTestCase.swift
//  WallboxMoviesTests
//
//  Created by alexei on 29/1/21.
//

import XCTest

class HomePresenterTestCase: XCTestCase {

    var presenter: HomePresenterProtocol!
    var interactor: HomeIntereactorMock!
    var view: HomeViewMock!
    var router: HomeRouterMock!
    
    override func setUp() {
        presenter = HomePresenter()
        interactor = HomeIntereactorMock()
        router = HomeRouterMock()
        view = HomeViewMock(presenter: presenter)
        
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
    }
    
    override func tearDown() {
        presenter = nil
        view = nil
        interactor = nil
        router = nil
    }
    
    func testShowLoadShows() {
        presenter.viewDidLoad()
        wait(for: 2)
        XCTAssertEqual(presenter.curretPage,2)
        XCTAssertTrue(interactor.requestCalled)
        XCTAssertTrue(view.tvShowsLoaded)
        XCTAssertFalse(view.isLoading)
    }
    
    func testDidSelectShow() {
        presenter.viewDidLoad()
        wait(for: 2)
        XCTAssertTrue(interactor.requestCalled)
        let tvShow = Show(name: "Test Show", image: ShowImage(medium: "", original: ""), rating: Rating(average: 5.0), genres: ["Comdy"], summary: "summary test")
        presenter.goToDetail(with: tvShow)
        XCTAssertTrue(view.tvShowsLoaded)
        XCTAssertTrue(router.tvShowDetailSuccess)
    }
    

}
